/**
  * Created by alex on 29/01/17
  **/
package object words {

  type Symbol = String
  type Element = String
  type Word = Seq[(Symbol, Element)]

}
