package elements

import java.io.{File, FileWriter, PrintWriter}

import words._

import scala.io.Source

/**
  * Created by alex on 28/01/17
  **/
object Application extends App {

  val allElements: Map[Symbol, Element] = Elements()

  val wordsBuilder: WordsBuilder = new WordsBuilder(allElements)

  val elementWords: Seq[(String, Word)] = for {
    el <- allElements.values.toSeq.sorted
    word <- wordsBuilder.lookup(el)
  } yield {
    el -> word
  }

  val elements: Seq[String] = elementWords.map(_._1).distinct.sorted
  val wordsByElement = elementWords.groupBy(_._1).mapValues(_.map(_._2))

  val rows = elements.map { element =>
    val words: Seq[Word] = wordsByElement.get(element).toSeq.flatten
    Seq(element, words.map(_.map(_._1).mkString(", ")).mkString("<br/>"), words.map(_.map(_._2).mkString(", ")).mkString("<br/>"))
  }

  val readmeFile = new File("README.md")
  val preamble = Source.fromFile(readmeFile).getLines().takeWhile(!_.startsWith("###")).mkString("\n")

  val writer = new PrintWriter(new FileWriter(readmeFile))
  //val writer = new PrintWriter(System.out)

  writer.println(preamble)

  elements.foreach { element =>
    writer.println(s"### $element")
    writer.println()
    writer.println("|Symbols|Elements|")
    writer.println("|:------|:-------|")
    wordsByElement(element).foreach { word =>
      val symbols: String = word.map(w => w._1).mkString(", ")
      val elements: String = word.map(w => w._2).mkString(", ")
      writer.println(Seq(symbols, elements).mkString("|", "|", "|"))
    }
    writer.println()
  }

  writer.close()
}
