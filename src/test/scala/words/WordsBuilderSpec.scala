package words

import org.scalatest.{MustMatchers, WordSpec}

/**
  * Created by alex on 26/01/17
  **/
class WordsBuilderSpec extends WordSpec with MustMatchers {

  val A: (String, String) = "a" -> "A"
  val AL: (String, String) = "al" -> "AL"
  val L: (String, String) = "l" -> "L"
  val LE: (String, String) = "le" -> "LE"
  val E: (String, String) = "e" -> "E"
  val EX: (String, String) = "ex" -> "EX"

  val elementsBySymbol: Map[String, String] = Seq(A, AL, L, LE, E, EX).toMap
  val wordsBuilder: WordsBuilder = new WordsBuilder(elementsBySymbol)

  "alex" should {
    "be constructable in two ways" in {
      wordsBuilder.lookup("alex") must contain theSameElementsAs Seq(Seq(AL, EX), Seq(A, L, EX))
    }
  }

  "lex" should {
    "be constructable in one way" in {
      wordsBuilder.lookup("lex") must contain theSameElementsAs Seq(Seq(L, EX))
    }
  }

  "alx" should {
    "not be constructable" in {
      wordsBuilder.lookup("alx") must contain theSameElementsAs Seq.empty
    }
  }

  "alle" should {
    "be constructable in four ways" in {
      wordsBuilder.lookup("alle") must contain theSameElementsAs Seq(
        Seq(A, L, L, E),
        Seq(AL, LE),
        Seq(AL, L, E),
        Seq(A, L, LE))
    }
  }
}
